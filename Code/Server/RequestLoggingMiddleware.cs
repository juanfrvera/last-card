﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Server
{
    public class RequestLoggingMiddleware
    {
        public readonly RequestDelegate next;
        private static DateTime dummy;

        public RequestLoggingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            // Execute this once a day
            if (dummy != DateTime.Today)
            {
                dummy = DateTime.Today;
                DeleteOldFiles(7);
            }

            // get data from the request
            string response = $"IP: {context.Request.Host.Host} || Path: {context.Request.Path.Value} || Method: {context.Request.Method} ";
            
            await next(context);
            
            // get data from the response
            string statusCode = context.Response.StatusCode.ToString();

            // format the data to be stored in a single line in a file
            response = DateTime.Now.ToString() + " -> " + response + "|| Status code: " + statusCode;

            // write (append) the line to the file created today
            string filePath = Environment.CurrentDirectory + "\\" + FormatDate(DateTime.Today) + "_ServerLog.txt";
            using StreamWriter file = new StreamWriter(filePath, true);
            file.WriteLine(response);
        }

        private string FormatDate(DateTime date)
        {
            return $"{date.Day}-{date.Month}-{date.Year}";
        }

        private void DeleteOldFiles(int daysToKeep)
        {
            string[] files = Directory.GetFiles(Environment.CurrentDirectory);

            foreach(string file in files)
            {
                if (file.EndsWith("_ServerLog.txt"))
                {
                    FileInfo fi = new FileInfo(file);
                    if (fi.CreationTime <= DateTime.Now.AddDays(-daysToKeep)) // Keep only newests files (last X days)
                    {
                        fi.Delete();
                    }
                }
            }
        }
    }
}
