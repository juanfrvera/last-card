﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.DTOs
{
    public class RoomBaseDTO
    {
        public string TrelloBoardID { get; set; }
        public string Link { get; set; }
    }
}
