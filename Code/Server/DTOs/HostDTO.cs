﻿namespace Server.DTOs
{
    public class HostDTO
    {
        public string Id { get; set; }
        public string Username { get; set; }
    }
}
