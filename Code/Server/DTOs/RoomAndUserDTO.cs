﻿namespace Server.DTOs
{
    public class RoomAndUserDTO
    {
        public RoomBaseDTO Room { get; set; }
        public UserDTO User { get; set; }
    }
}
