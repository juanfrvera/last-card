﻿namespace Server.DTOs
{
    public class TargetDTO
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public float? FinalResult { get; set; }
        public string AllowedResult { get; set; }
    }
}
