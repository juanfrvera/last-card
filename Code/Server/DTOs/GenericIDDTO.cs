﻿namespace Server.DTOs
{
    public class GenericIDDTO
    {
        public string ID { get; set; }
    }
}
