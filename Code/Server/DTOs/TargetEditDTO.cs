﻿namespace Server.DTOs
{
    public class TargetEditDTO
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ResultType { get; set; }
    }
}
