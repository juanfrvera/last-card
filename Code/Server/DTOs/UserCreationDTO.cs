﻿namespace Server.DTOs
{
    public class UserCreationDTO
    {
        public string Name { get; set; }
        public string Avatar { get; set; }
    }
}
