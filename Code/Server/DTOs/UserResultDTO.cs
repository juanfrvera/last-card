﻿namespace Server.DTOs
{
    public class UserResultDTO
    {
        public UserDTO User { get; set; }
        public string Value { get; set; }
    }
}
