﻿using System;

namespace Server.DTOs
{
    public class RoomDTO: RoomBaseDTO
    {
        public string RoomID { get; set; }
        public DateTime Date { get; set; }
    }
}