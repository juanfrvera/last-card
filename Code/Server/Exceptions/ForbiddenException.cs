﻿using System;

namespace Server.Exceptions
{
    public class ForbiddenException: Exception
    {
        public ForbiddenException() : base("You don't have permission. Access denied.") { }
        public ForbiddenException(Exception inner) : base("You don't have permission. Access denied.", inner) { }
    }
}
