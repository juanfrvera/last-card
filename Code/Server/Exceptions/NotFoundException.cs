﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Exceptions
{
    public class NotFoundException: Exception
    {
        public NotFoundException(string message) : base(message) { }
        public NotFoundException() : base("Resource not found.") { }
        public NotFoundException(Exception inner) : base("Resourse not found.", inner) { }
    }
}
