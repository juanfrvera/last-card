﻿using System;

namespace Server.Exceptions
{
    public class UnauthorizedException: Exception
    {
        public UnauthorizedException(string message) : base(message) { }
        public UnauthorizedException() : base("The User ID is missing or invalid.") { }
        public UnauthorizedException(Exception inner): base("The User ID is missing or invalid.", inner) { }
    }
}
