﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Server.Services;
using Server.DTOs;
using System.Collections.Generic;

namespace Server.Controllers
{
    [Route("/room")]
    [ApiController]
    public class RoomController : ControllerBase
    {
        private readonly RoomService _roomService;

        public RoomController(RoomService roomService)
        {
            _roomService = roomService;
        }

        private string UID
        { get { return Request.HttpContext.Items["uid"] as string; } }

        // Create a normal room
        // POST: /room 
        [HttpPost]
        public async Task<IActionResult> CreateRoom([FromBody] UserCreationDTO creationData, [FromHeader] string signalrid)
        {

            await _roomService.DisconnectFromPreviousRoom(this.UID, signalrid);
            RoomAndUserDTO result = await _roomService.CreateRoom(this.UID, creationData, signalrid);

            return Created("room/" + result.Room.Link, result); // Status code: 201
        }

        // Get a room
        // GET: /room
        // Return Ok (200) if room exists and is joineable, 404 if not exist
        [HttpGet]
        public IActionResult GetRoom([FromHeader] string link)
        {
            _roomService.SearchRoom(link);
            return Ok(); // Status code: 200
        }

        // Get the host ID
        // GET: /room/host
        // Return Ok (200) if room exists and is joineable, 404 if not exist
        [HttpGet("host")]
        public async Task<IActionResult> GetHost([FromHeader] string link)
        {
            long result = await _roomService.GetHost(link);
            return Ok(result); // Status code: 200
        }

        // Get all users in a room
        // GET: /room/user
        [HttpGet("user")]
        public async Task<IActionResult> GetRoomUsers([FromHeader] string link)
        {
            IList<UserDTO> result = await _roomService.GetRoomUsers(link);
            return Ok(result);
        }

        // Delete a room
        // DELETE /room
        [HttpDelete]
        public async Task<IActionResult> DeleteRoom([FromHeader] string link, [FromHeader] string signalrid)
        {
                await _roomService.DeleteRoom(this.UID, link, signalrid);
                return NoContent();
        }

        // Transfer Privileges
        // PUT /room/transferhost
        [HttpPut("transferhost")]
        public async Task<IActionResult> TransferPrivileges([FromBody] HostDTO newHost, [FromHeader] string link)
        {
                HostDTO result = await _roomService.TransferPrivileges(this.UID, link, newHost);
                return Ok(result);
        }

        // Join Room
        // PUT /room
        [HttpPut]
        public async Task<IActionResult> JoinRoom([FromBody] UserDTO user, [FromHeader] string link, [FromHeader] string signalrid)
        {
            await _roomService.DisconnectFromPreviousRoom(this.UID, signalrid);
            RoomAndUserDTO result = await _roomService.JoinRoom(this.UID, link, signalrid, user);
            return Ok(result);
        }
    }
}
