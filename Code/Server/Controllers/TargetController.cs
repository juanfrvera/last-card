﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Server.DTOs;
using Server.Services;
using System.Threading.Tasks;

namespace Server.Controllers
{
    [Route("/target")]
    [ApiController]
    public class TargetController : ControllerBase
    {
        private readonly TargetService _targetService;
        private readonly RoomService _roomService;
        private readonly IHubContext<Hubs.SignalRHub> _hub;

        public TargetController(TargetService targetService, RoomService roomService, IHubContext<Hubs.SignalRHub> hub)
        {
            _targetService = targetService;
         _roomService = roomService;
         _hub = hub;
        }

        private string UID
        { get { return Request.HttpContext.Items["uid"] as string; } }

        // Get all targets
        // GET: /target
        [HttpGet()]
        public async Task<IActionResult> GetTarget([FromHeader] string link)
        {
            var result = await _targetService.GetTargets(this.UID, link);
            return Ok(result); // Status code: 200
        }

        // Get a single target by ID
        // GET: /target/id
        [HttpGet("{targetID}")]
        public async Task<IActionResult> GetTarget([FromHeader] string link, [FromRoute] string targetID)
        {
            var result = await _targetService.GetTarget(this.UID, link, targetID);
            return Ok(result); // Status code: 200
        }

        // Get the current target for a room
        // GET: /target/current
        [HttpGet("current")]
        public async Task<IActionResult> GetCurrentTarget([FromHeader] string link)
        {
            var result = await _targetService.GetCurrentTargetId(this.UID, link);
            return Ok(result); // Status code: 200
        }

        // Start a new scoring round!
        // POST: /target/start/id
        [HttpPost("start")]
        public async Task<IActionResult> StartRound([FromHeader] string link, [FromBody] GenericIDDTO targetId, [FromHeader] string signalrid)
        {
            long result = await _targetService.StartScoringRound(this.UID, link, targetId.ID);

            // Send the clients in the room that a target has started
            await _hub.Clients.GroupExcept(link, signalrid).SendAsync("startround", result);

            return Ok(result); // Status code: 200
        }

        // Create a target
        // POST: /target
        [HttpPost()]
        public async Task<IActionResult> CreateTarget([FromHeader] string link, [FromBody] TargetDTO target, [FromHeader] string signalrid)
        {
            target.Id = null;
            TargetDTO result = await _targetService.CreateTarget(this.UID, link, target);

            // Send the clients in the room that a newtarget has been created
            await _hub.Clients.GroupExcept(link, signalrid).SendAsync("newtarget", result);

            return Created(result.Id, result); // Status code: 201
        }

        // Modify a target
        // PUT: /target/id
        [HttpPut("{targetID}")]
        public async Task<IActionResult> ModifyTarget([FromHeader] string link, [FromRoute] string targetID, [FromBody] TargetEditDTO targetEditDTO, [FromHeader] string signalrid)
        {
            TargetDTO target = new TargetDTO
            {
                Id = targetID,
                Title = targetEditDTO.Title,
                Description = targetEditDTO.Description
            };
                
            await _targetService.ModifyTarget(this.UID, link, target);

            // Send the clients in the room that a target has been edited
            await _hub.Clients.GroupExcept(link, signalrid).SendAsync("targetedit", target);

            return NoContent(); // Status code: 204
        }

        // Delete a target
        // DELETE /target/id
        [HttpDelete("{targetID}")]
        public async Task<IActionResult> DeleteTarget([FromHeader] string link, [FromRoute] string targetID, [FromHeader] string signalrid)
        {
            await _targetService.DeleteTarget(this.UID, link, targetID);

            // Send the clients in the room that a target has been deleted
            await _hub.Clients.GroupExcept(link, signalrid).SendAsync("targetdelete", targetID);

            return NoContent();
        }

        // Set user result to a target
        // POST: target/targetID/userResult
        [HttpPost("score")]
        public async Task<IActionResult> SetUserScore([FromHeader] string link, [FromBody] ScoreDTO scoreDTO)
        {
         var currentTargetId = await _targetService.GetCurrentTargetId(this.UID, link);

         if (currentTargetId.HasValue)
         {
            TargetDTO result = await _targetService.SetUserScore(this.UID, link, currentTargetId.Value, scoreDTO.Value);

            return Created(result.Id, result);
         }
         else
         {
            throw new System.Exception("Not target in round");
         }
      }

        [HttpPost("finish")]
        public async Task<IActionResult> FinishScoring([FromHeader] string link, [FromHeader] string signalrid)
        {
         var targetId = await this._targetService.GetCurrentTargetId(UID,link);

         if (targetId.HasValue)
         {
            var finalResult = _targetService.CalculateFinalResult(this.UID, link, targetId.Value);

            await _targetService.SetFinalResult(UID, link, targetId.Value, finalResult);
            await _hub.Clients.GroupExcept(link, signalrid).SendAsync("finishround");
         }
         else
         {
            throw new System.Exception("Not target in round");
         }

         return Ok();
        }

          /// <summary>
          /// Exits the current round
          /// </summary>
        [HttpPost("exit")]
        public async Task<IActionResult> ExitRound([FromHeader] string link, [FromHeader] string signalrid)
        {
            bool result = await _roomService.ExitRound(this.UID, link);
            await _hub.Clients.GroupExcept(link, signalrid).SendAsync("exitround");
            return Created("", result);
        }


      [HttpPost("showresult")]
      public async Task<IActionResult> ShowRoundResult([FromHeader] string link,[FromHeader] string signalrid)
      {
         var targetId = await _targetService.GetCurrentTargetId(UID, link);
         
         if(targetId == null) throw new System.Exception("Not target in round");

         var target = await _targetService.GetTarget(UID, link, targetId.Value.ToString());

         if (target.FinalResult == null) throw new System.Exception("There is no result on the target");

         await _hub.Clients.GroupExcept(link, signalrid).SendAsync("showresult", target.FinalResult);

         return Ok(target.FinalResult);
      }
   }
}
