﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Server.DTOs;
using Server.Services;

namespace Server.Controllers
{
    [Route("/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        private string UID
        {
            get { return Request.HttpContext.Items["uid"] as string; }
        }

        // Get a user
        // GET user/id
        [HttpGet("{userID}")]
        public async Task<IActionResult> GetUser([FromRoute] string userID)
        {
            UserDTO result = await _userService.GetUser(userID);
            return Ok(result); //Status code: 200
        }

        // Create a user
        // POST /user
        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] UserDTO userDTO, [FromHeader] string signalrid)
        {
            UserDTO result = await _userService.CreateUser(this.UID, userDTO, signalrid);
            return Created("user/" + result.Id, result);
        }

        // Update user
        // PUT /user/id
        [HttpPut("{userID}")]
        public async Task<IActionResult> UpdateUser([FromRoute] string userID, [FromBody] UserDTO userDTO, [FromHeader] string signalrid)
        {
            userDTO.Id = userID;
            await _userService.UpdateUser(this.UID, userDTO, signalrid);
            return NoContent(); //204
        }

        // Delete user
        // DELETE /user/id
        [HttpDelete("{userID}")]
        public async Task<IActionResult> DeleteUser([FromRoute] string userID, [FromHeader] string signalrid)
        {
            await _userService.DeleteUser(this.UID, userID, signalrid);
            return NoContent();
        }
    }
}
