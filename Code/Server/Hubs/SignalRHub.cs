﻿using Microsoft.AspNetCore.SignalR;
using Server.Services;
using System;
using System.Threading.Tasks;
using Server.DTOs;

namespace Server.Hubs
{
    public class SignalRHub : Hub
    {
        private readonly UserService _userService;

        public SignalRHub(UserService userService)
        {
            _userService = userService;
        }

        public string GetConnectionId()
        {
            return Context.ConnectionId;
        }

        public async override Task OnDisconnectedAsync(Exception exception)
        {
            string conID = Context.ConnectionId;
            RoomAndUserDTO userInRoom = _userService.GetRoomAndUserByConID(conID);

            if(userInRoom != null)
            {
                await _userService.RemoveUserFromRoomByID(userInRoom.User.Id, userInRoom.Room.Link, conID);
            }

            await base.OnDisconnectedAsync(exception);
        }
   }
}
