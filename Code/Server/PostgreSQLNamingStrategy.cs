﻿using NHibernate.Cfg;
using System;

namespace Server
{
    // To allow uppercase and camelcase in PostgreSQL
    // By default postgres converts names to lowercase
    public class PostgreSQLNamingStrategy: INamingStrategy
    {
        public string ClassToTableName(string className)
        {
            return DoubleQuote(className);
        }
        public string PropertyToColumnName(string propertyName)
        {
            return DoubleQuote(propertyName);
        }
        public string TableName(string tableName)
        {
            return DoubleQuote(tableName);
        }
        public string ColumnName(string columnName)
        {
            return DoubleQuote(columnName);
        }
        public string PropertyToTableName(string className,
                                          string propertyName)
        {
            return DoubleQuote(propertyName);
        }
        public string LogicalColumnName(string columnName,
                                        string propertyName)
        {
            return String.IsNullOrWhiteSpace(columnName) ?
                DoubleQuote(propertyName) :
                DoubleQuote(columnName);
        }
        private static string DoubleQuote(string raw)
        {
            // Removing both single and double quotes:
            raw = raw.Replace("\"", "");
            raw = raw.Replace("`", "");
            return String.Format("\"{0}\"", raw);
        }
    }
}
