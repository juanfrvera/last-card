﻿using Microsoft.AspNetCore.Http;
using NHibernate;
using NHibernate.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server
{
    public class SessionTransactionPerRequestMiddleware
    {
        private readonly RequestDelegate next;

        public SessionTransactionPerRequestMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext httpContext, NHibernate.ISession session)
        {
            if (session == null) throw new ArgumentNullException(nameof(session));

            try
            {
                session.BeginTransaction();
                await next.Invoke(httpContext);
                if (session.GetCurrentTransaction() != null)
                    session.GetCurrentTransaction().Commit();
            }
            catch (Exception e)
            {
                if (session.GetCurrentTransaction() != null)
                    session.GetCurrentTransaction().Rollback();
                throw e;
            }
            finally
            {
                session.Close();
            }
        }
    }
}
