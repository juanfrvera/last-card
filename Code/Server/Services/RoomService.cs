﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NHibernate;
using Server.DataComponents;
using Server.DTOs;
using Server.Entities;
using Server.Exceptions;

namespace Server.Services
{
    public class RoomService
    {
        private readonly ISessionFactory _sessionFactory;
        private readonly UserService _userService;

        public RoomService(ISessionFactory sessionFactory, UserService userService)
        {
            _sessionFactory = sessionFactory;
            _userService = userService;
        }

        internal ISession CurrentSession
        {
            get { return this._sessionFactory.GetCurrentSession(); }
        }

        internal async Task DisconnectFromPreviousRoom(string uid, string signalrid)
        {
            // Remove the user for a previous room, informing other members
            Room room = await FetchPreviosRoomFrom(uid);
            if (room.Link != null)
            {
                await _userService.RemoveUserFromRoomByUID(uid, room.Link, signalrid);
            }
        }

        /// <summary>
        /// Creates a new Room
        /// </summary>
        /// <param name="creationRoom">Room data</param>
        /// <returns>Room and Host</returns>
        internal async Task<RoomAndUserDTO> CreateRoom(string uid, UserCreationDTO creationData, string signalrid)
        {
            User host = new User();

            // Get or Set the host
            try
            {
                // If the user have a previous valid UID we simply update his information (name and avatar)
                host = CurrentSession.Query<User>().Where(c => c.FirebaseUID == uid).First();
                DeletePreviousRoom(uid, signalrid);
            }
            catch(InvalidOperationException)
            {
                host.FirebaseUID = uid;
            }

            // Update host info
            host.Name = creationData.Name;
            host.Avatar = String.IsNullOrEmpty(creationData.Avatar) ? Avatar.DefaultAvatar() : new Avatar { Picture = creationData.Avatar };
            host.SignalRConnectionId = signalrid;

            // Save or update the host
            await CurrentSession.SaveOrUpdateAsync(host);

            // Generate the link to access the room
            string link = Room.GenerateLink();
            while (CurrentSession.Query<Room>().Where(c => c.Link == link).Count() != 0)
            {
                link = Room.GenerateLink();
            }

            // Generate the new room
            Room newRoom = new Room
            {
                Date = DateTime.Now,
                Link = link,
                Host = host
            };

            // Save the room
            await CurrentSession.SaveAsync(newRoom);
            await _userService.UserJoinRoom(host, newRoom);

            // Return data
            RoomBaseDTO room = new RoomBaseDTO
            {
                Link = link
            };

            UserDTO user = new UserDTO
            {
                Id = host.Id.ToString(),
                Avatar = host.Avatar.Picture,
                Name = host.Name
            };

            return new RoomAndUserDTO { Room = room, User = user };
        }

        internal Task<Room> FetchPreviosRoomFrom(string uid)
        {
            Room result = new Room();

            UserRoom userInRoom = CurrentSession.Query<UserRoom>().Where(x => x.User.FirebaseUID == uid).FirstOrDefault();
            if (userInRoom != null)
            {
                result = userInRoom.Room;
            }

            return Task.FromResult(result);
        }

        internal async Task<bool> ExitRound(string uid, string link)
        {
            Room room = CurrentSession.Query<Room>().Where(c => c.Link == link).FirstOrDefault();
            if (uid != room.Host.FirebaseUID)
            {
                throw new ForbiddenException();
            }

            room.CurrentTarget = null;


            bool result;
            {
                try
                {
                    await CurrentSession.UpdateAsync(room);
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }

            return result;
        }

        internal Task<long> GetHost(string link)
        {
            return Task.FromResult(SearchRoom(link).Host.Id);
        }

        internal Task<IList<UserDTO>> GetRoomUsers(string link)
        {
            IList<UserDTO> result = new List<UserDTO>();

            foreach (UserRoom ur in CurrentSession.Query<UserRoom>().Where(c => c.Room.Link == link).ToList())
            {
                result.Add(ur.User.ToDTO());
            }
            return Task.FromResult(result);
        }

        /// <summary>
        /// Obtein the Room and Host data from a Link
        /// </summary>
        /// <returns>Room and User data</returns>
        internal Task<RoomAndUserDTO> GetRoom(string link)
        {
            Room room = SearchRoom(link);
            
            RoomAndUserDTO result = new RoomAndUserDTO
            {
                Room = new RoomDTO
                {
                    RoomID = room.Id.ToString(),
                    TrelloBoardID = room.TrelloBoardId == null ? String.Empty : room.TrelloBoardId.ToString(),
                    Date = room.Date,
                    Link = room.Link
                },
                User = new UserDTO
                {
                    Id = room.Host.Id.ToString(),
                    Avatar = room.Host.Avatar.Picture,
                    Name = room.Host.Name
                }
            };

            return Task.FromResult<RoomAndUserDTO>(result);
        }

        internal async Task<RoomAndUserDTO> JoinRoom(string uid, string link, string connID, UserDTO userDTO)
        {
            Room room = SearchRoom(link);
            User user = new User();

            try
            {
                // If the user have a previous valid UID we simply update his information
                user = CurrentSession.Query<User>().Where(c => c.FirebaseUID == uid).First();
            }
            catch (InvalidOperationException)
            {
                user.FirebaseUID = uid;
            }

            // Update host info
            user.Name = userDTO.Name;
            user.Avatar = String.IsNullOrEmpty(userDTO.Avatar) ? Avatar.DefaultAvatar() : new Avatar { Picture = userDTO.Avatar };
            user.SignalRConnectionId = connID;

            // Save or update the user
            await CurrentSession.SaveOrUpdateAsync(user);
            await _userService.UserJoinRoom(user, room);

            RoomAndUserDTO result = new RoomAndUserDTO
            {
                Room = room.ToDTO(),
                User = user.ToDTO()
            };

            return result;
        }

        internal async Task DeleteRoom(string uid, string link, string signalrid)
        {
            Room room = SearchRoom(link);
            if(uid != room.Host.FirebaseUID)
            {
                throw new ForbiddenException();
            }

            await _userService.RemoveUserFromRoomByUID(uid, link, signalrid);

            List<Target> targetList = CurrentSession.Query<Target>().Where(c => c.Room == room).ToList();
            foreach(Target t in targetList)
            {
                t.Room = null;
                t.FinalResult = null;
                await CurrentSession.UpdateAsync(t);
                await CurrentSession.DeleteAsync(t);
            }

            await CurrentSession.DeleteAsync(room);
        }

        internal Task<HostDTO> TransferPrivileges(string uid, string link, HostDTO newHost)
        {
            Room room = SearchRoom(link);

            if (uid != room.Host.FirebaseUID)
            {
                throw new ForbiddenException();
            }

            long searchID = Convert.ToInt64(newHost.Id);
            User user = CurrentSession.Query<User>().Where(c => c.Id == searchID).FirstOrDefault();

            room.Host = user;
            CurrentSession.SaveOrUpdate(room);

            HostDTO host = new HostDTO
            {
                Id = user.Id.ToString(),
                Username = user.Name
            };

            return Task.FromResult<HostDTO>(host);
        }

        internal Room SearchRoom(string link)
        {
            Room room = CurrentSession.Query<Room>().Where(c => c.Link == link).FirstOrDefault() ?? throw new NotFoundException();

            return room;
        }

        internal async void DeletePreviousRoom(string uid, string connID)
        {
            Room room = CurrentSession.Query<Room>().Where(c => c.Host.FirebaseUID == uid).FirstOrDefault();
            if (room != null)
            {
                await this.DeleteRoom(uid, room.Link, connID);
            }
        }
    }
}
