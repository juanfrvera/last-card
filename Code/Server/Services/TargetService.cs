﻿using NHibernate;
using Server.DataComponents;
using Server.DTOs;
using Server.Entities;
using Server.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class TargetService
    {
        private readonly ISessionFactory _sessionFactory;
        private readonly RoomService _roomService;

        public TargetService(ISessionFactory sessionFactory, RoomService roomService)
        {
            _sessionFactory = sessionFactory;
            _roomService = roomService;
        }

        internal ISession CurrentSession
        {
            get { return this._sessionFactory.GetCurrentSession(); }
        }

        internal Task<IList<TargetDTO>> GetTargets(string uid, string link)
        {
            Room room = CurrentSession.Query<Room>().Where(c => c.Link == link).FirstOrDefault() ?? throw new NotFoundException();
            UserRoom ur = CurrentSession.Query<UserRoom>().Where(r => r.Room == room && r.User.FirebaseUID == uid).FirstOrDefault() ?? throw new ForbiddenException();

            IList<TargetDTO> result = new List<TargetDTO>();
            foreach(Target t in CurrentSession.Query<Target>().Where(c => c.Room.Link == link).ToList())
            {
                TargetDTO targetDTO = t.ToDTO();
                result.Add(targetDTO);
            }

            return Task.FromResult(result);
        }

        internal Task<TargetDTO> GetTarget(string uid, string link, string targetID)
        {
            Room room = CurrentSession.Query<Room>().Where(c => c.Link == link).FirstOrDefault() ?? throw new NotFoundException();
            UserRoom ur = CurrentSession.Query<UserRoom>().Where(r => r.Room == room && r.User.FirebaseUID == uid).FirstOrDefault() ?? throw new ForbiddenException();

            long idToSearch = Convert.ToInt64(targetID);
            Target t = CurrentSession.Query<Target>().Where(c => (c.Room.Link == link && c.Id == idToSearch)).FirstOrDefault();

            return Task.FromResult(t.ToDTO());
        }

        internal Task<long?> GetCurrentTargetId(string uid, string link)
        {
            Room room = CurrentSession.Query<Room>().Where(c => c.Link == link).FirstOrDefault() ?? throw new NotFoundException();
            UserRoom ur = CurrentSession.Query<UserRoom>().Where(r => r.Room == room && r.User.FirebaseUID == uid).FirstOrDefault() ?? throw new ForbiddenException();

            return Task.FromResult((long?)room.CurrentTarget?.Id ?? null);
        }

        internal async Task<long> StartScoringRound(string uid, string link, string targetID)
        {
            Room room = CurrentSession.Query<Room>().Where(c => c.Link == link).FirstOrDefault() ?? throw new NotFoundException();
            if (room.Host.FirebaseUID != uid)
            {
                throw new ForbiddenException();
            }

            long searchID = Convert.ToInt64(targetID);
            Target target = CurrentSession.Query<Target>().Where(c => c.Id == searchID).FirstOrDefault() ?? throw new NotFoundException();

            room.CurrentTarget = target;
            await CurrentSession.UpdateAsync(room);

            return target.Id;
        }

        internal Task<TargetDTO> CreateTarget(string uid, string link, TargetDTO targetDTO)
        {
            Room room = CurrentSession.Query<Room>().Where(c => c.Link == link).FirstOrDefault() ?? throw new NotFoundException();
            if (room.Host.FirebaseUID != uid)
            {
                throw new ForbiddenException();
            }

            Target t = DTOtoTarget(targetDTO, link);
            CurrentSession.SaveOrUpdate(t);
            return Task.FromResult(t.ToDTO());
        }

        internal async Task ModifyTarget(string uid, string link, TargetDTO target)
        {
            Room room = CurrentSession.Query<Room>().Where(c => c.Link == link).FirstOrDefault() ?? throw new NotFoundException();
            if (room.Host.FirebaseUID != uid)
            {
                throw new ForbiddenException();
            }

            await CurrentSession.UpdateAsync(DTOtoTarget(target, link));
            return;
        }

        internal async Task DeleteTarget(string uid, string link, string targetID)
        {
            Room room = CurrentSession.Query<Room>().Where(c => c.Link == link).FirstOrDefault() ?? throw new NotFoundException();
            if (room.Host.FirebaseUID != uid)
            {
                throw new ForbiddenException();
            }

            long searchID = Convert.ToInt64(targetID);
            Target target = CurrentSession.Query<Target>().Where(c => (c.Room.Link == link && c.Id == searchID)).FirstOrDefault();
            await CurrentSession.DeleteAsync(target);

            return;
        }

        internal Task SetFinalResult(string uid, string link, long targetId, float finalResult)
        {
            Room room = CurrentSession.Query<Room>().Where(c => c.Link == link).FirstOrDefault() ?? throw new NotFoundException();
            if (room.Host.FirebaseUID != uid)
            {
                throw new ForbiddenException();
            }

            Target target = CurrentSession.Query<Target>().Where(c => (c.Room.Link == link && c.Id == targetId)).FirstOrDefault();
            target.FinalResult = new Score { Value = finalResult };

            CurrentSession.Update(target);
            return Task.CompletedTask;
        }

        private Target DTOtoTarget(TargetDTO t, string link)
        {
            Room room = _roomService.SearchRoom(link);

            ResultType resultType = ResultType.Numeric;
            try
            {
                resultType = (ResultType)Enum.Parse(typeof(ResultType), t.AllowedResult, false);
            }
            catch (Exception) { }

            Target result = new Target
            {
                Id = Convert.ToInt64(t.Id),
                Title = t.Title,
                Description = t.Description,
                AllowedResultType = CurrentSession.Query<AllowedResultType>().Where(c => c.Name == resultType).FirstOrDefault(),
                FinalResult = t.FinalResult.HasValue ? new Score { Value = t.FinalResult.Value } : null,
                Room = room
            };

            return result;
        }

        internal async Task<TargetDTO> SetUserScore(string uid, string link, long targetId, float value)
        {
            Room room = CurrentSession.Query<Room>().Where(c => c.Link == link).FirstOrDefault() ?? throw new NotFoundException();
            UserRoom ur = CurrentSession.Query<UserRoom>().Where(r => r.Room == room && r.User.FirebaseUID == uid).FirstOrDefault() ?? throw new ForbiddenException();

            Score score = CurrentSession.Query<Score>().Where(s => s.Target.Id == targetId && s.User.FirebaseUID == uid).FirstOrDefault();

            if(score == null)
         {
            score = new Score
            {
               User = CurrentSession.Query<User>().Where(c => c.Id == ur.User.Id).FirstOrDefault() ?? throw new NotFoundException(),
               Target = CurrentSession.Query<Target>().Where(c => (c.Id == targetId && c.Room.Link == link)).FirstOrDefault() ?? throw new NotFoundException(),
               Value = value
            };
         }
            // If there is already an score, just update it
         else
         {
            score.Value = value;
         }
            

            await CurrentSession.SaveOrUpdateAsync(score);

            return score.Target.ToDTO();
        }

        internal float CalculateFinalResult(string uid, string link, long targetID)
        {
            Room room = CurrentSession.Query<Room>().Where(c => c.Link == link).FirstOrDefault() ?? throw new NotFoundException();
            if (room.Host.FirebaseUID != uid)
            {
                throw new ForbiddenException();
            }

            var scoreList = CurrentSession.Query<Score>().Where(c => c.Target.Room.Link == link && c.Target.Id == targetID).ToList();
         var score = 0f;

            foreach (Score r in scoreList)
            {
            score += r.Value;
            }

            // Return the average
         return (score / scoreList.Count);
        }
    }
}
