﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Server.Entities;
using Server.DTOs;
using Server.DataComponents;
using Server.Exceptions;
using Microsoft.AspNetCore.SignalR;

namespace Server.Services
{
    public class UserService
    {
        private readonly ISessionFactory _sessionFactory;
        private readonly IHubContext<Hubs.SignalRHub> _hub;

        public UserService(ISessionFactory sessionFactory, IHubContext<Hubs.SignalRHub> hub)
        {
            _sessionFactory = sessionFactory;
            _hub = hub;
        }

        internal ISession CurrentSession
        {
            get { return this._sessionFactory.GetCurrentSession(); }
        }

        internal async Task<UserDTO> CreateUser(string uid, UserDTO userDTO, string signalrid)
        {
            User user = new User
            {
                Name = userDTO.Name,
                Avatar = String.IsNullOrEmpty(userDTO.Avatar) ? Avatar.DefaultAvatar() : new Avatar { Picture = userDTO.Avatar },
                FirebaseUID = uid,
                SignalRConnectionId = signalrid                
            };
            
            // trello User
            if (!String.IsNullOrEmpty(userDTO.TrelloID))
            {
                user.TrelloUserId = userDTO.TrelloID;
                // to complete..
            }

            await CurrentSession.SaveAsync(user);
            userDTO.Id = user.Id.ToString();
            return userDTO;
        }

        internal async Task UpdateUser(string uid, UserDTO userDTO, string signalrid)
        {
            User user = CurrentSession.Query<User>().Where(c => c.Id == Convert.ToInt64(userDTO.Id)).FirstOrDefault();

            if (uid == user.FirebaseUID)
            {
                user.Name = userDTO.Name;
                user.Avatar = String.IsNullOrEmpty(userDTO.Avatar) ? Avatar.DefaultAvatar() : new Avatar { Picture = userDTO.Avatar };
                user.SignalRConnectionId = signalrid;

                // If there is TrelloID, update it too
                if (!String.IsNullOrEmpty(userDTO.TrelloID))
                    user.TrelloUserId = userDTO.TrelloID;

                await CurrentSession.UpdateAsync(user);
            }
            else
            {
                throw new ForbiddenException();
            }
        }

        internal Task<UserDTO> GetUser(string userID)
        {
            User user = CurrentSession.Query<User>().Where(c => c.Id == Convert.ToInt64(userID)).FirstOrDefault() ?? throw new NotFoundException();
            return Task.FromResult(user.ToDTO());
        }

        internal Task<UserDTO> GetTrelloUser(string userTrelloID)
        {
            User user = CurrentSession.Query<User>().Where(c => c.TrelloUserId == userTrelloID).FirstOrDefault() ?? throw new NotFoundException();
            return Task.FromResult(user.ToDTO());
        }

        internal async Task DeleteUser(string uid, string userID, string signalrid)
        {
            long searchID = Convert.ToInt64(userID);
            User user = CurrentSession.Query<User>().Where(c => c.Id == searchID).FirstOrDefault() ?? throw new NotFoundException();
            
            if (uid == user.FirebaseUID)
            {
                UserRoom userInRoom = CurrentSession.Query<UserRoom>().Where(c => c.User.Id == user.Id).FirstOrDefault();
                if (userInRoom != null)
                {
                    await RemoveUserFromRoomByUID(uid, userInRoom.Room.Link, signalrid);
                }
                   
                await CurrentSession.DeleteAsync(user);
            }
            else
            {
                throw new ForbiddenException();
            }
        }

        public User DTOtoUser(UserDTO u)
        {
            User user = new User
            {
                Name = u.Name,
                TrelloUserId = String.IsNullOrEmpty(u.TrelloID) ? String.Empty : u.TrelloID,
                Avatar = String.IsNullOrEmpty(u.Avatar) ? Avatar.DefaultAvatar() : new Avatar { Picture = u.Avatar }
            };

            if (!String.IsNullOrEmpty(u.Id))
            {
                user.Id = Convert.ToInt64(u.Id);
            }

            return user;
        }

        public RoomAndUserDTO GetRoomAndUserByConID(string id)
        {
            UserRoom ur = CurrentSession.Query<UserRoom>().Where(c => c.User.SignalRConnectionId == id).FirstOrDefault();

            RoomAndUserDTO result = null;

            if (ur != null)
            {
               result = new RoomAndUserDTO
               {
                  Room = ur.Room.ToDTO(),
                  User = ur.User.ToDTO()
               };
            }

            return result;
        }

        public async Task RemoveUserFromRoomByUID(string uid, string link, string connID)
        {
            var userInRoom = CurrentSession.Query<UserRoom>().Where(c => c.User.FirebaseUID == uid && c.Room.Link == link).ToList();
            if (userInRoom.Count != 0)
            {
                foreach(UserRoom ur in userInRoom)
                {
                    ur.Room = null;
                    ur.User = null;
                    await CurrentSession.UpdateAsync(ur);
                    await CurrentSession.DeleteAsync(ur);
                }
            }
        }

        public async Task RemoveUserFromRoomByID(string id, string link, string connID)
        {
            long searchID = Convert.ToInt64(id);
            var userInRoom = CurrentSession.Query<UserRoom>().Where(c => c.User.Id == searchID && c.Room.Link == link).FirstOrDefault();
            if (userInRoom != null)
            {
                // SignalR send message to other clients that user is not more in the room
                await _hub.Clients.GroupExcept(link, connID).SendAsync("userdelete", userInRoom.User.Id);
                await _hub.Groups.RemoveFromGroupAsync(connID, link);

                userInRoom.Room = null;
                userInRoom.User = null;
                await CurrentSession.UpdateAsync(userInRoom);
                await CurrentSession.DeleteAsync(userInRoom);
            }
        }


        public async Task UserJoinRoom(User user, Room room)
        {
            UserRoom userInRoom = new UserRoom
            {
                Room = room,
                User = user
            };

            // Save the new user in the room
            await CurrentSession.SaveOrUpdateAsync(userInRoom);

            // Inform everybody the good news
            await _hub.Clients.Group(room.Link).SendAsync("newuser", user.ToDTO());
            await _hub.Groups.AddToGroupAsync(user.SignalRConnectionId, room.Link);

            return;
        }
    }
}
