﻿using Server.DTOs;
using System;

namespace Server.Entities
{
    public class Target: BaseEntity
    {   
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual Score FinalResult { get; set; }
        public virtual string TrelloTargetId { get; set; }
        public virtual AllowedResultType AllowedResultType { get; set; }
        public virtual Room Room { get; set; }

        protected internal virtual TargetDTO ToDTO()
        {
            return new TargetDTO
            {
                Id = this.Id.ToString(),
                Title = this.Title,
                Description = this.Description,
                FinalResult = this.FinalResult?.Value,
                AllowedResult = this.AllowedResultType.Name.ToString()
            };
        }
    }
}