﻿using Server.DataComponents;

namespace Server.Entities
{
    public class AllowedResultType: BaseEntity
    {
        public virtual ResultType Name { get; set; }

        public override string ToString()
        {
            return Name.ToString();
        }
    }
}
