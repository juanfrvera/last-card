﻿using Server.DataComponents;
using Server.DTOs;

namespace Server.Entities
{
    public class User: BaseEntity
    {
        public virtual string Name { get; set; }
        public virtual Avatar Avatar { get; set; }
        public virtual string FirebaseUID { get; set; }
        public virtual string TrelloUserId { get; set; }
        public virtual string SignalRConnectionId { get; set; }

        protected internal virtual UserDTO ToDTO()
        {
            return new UserDTO
            {
                Id = this.Id.ToString(),
                Avatar = this.Avatar.Picture,
                TrelloID = this.TrelloUserId,
                Name = this.Name
            };
        }
    }
}
