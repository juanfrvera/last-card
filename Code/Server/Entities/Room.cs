﻿using Microsoft.AspNetCore.Http;
using Server.DTOs;
using System;

namespace Server.Entities
{
    public class Room: BaseEntity
    {
        public virtual DateTime Date { get; set; }
        public virtual string Link { get; set; }
        public virtual string TrelloBoardId { get; set; }
        public virtual User Host { get; set; }
        public virtual Target CurrentTarget { get; set; }

        protected internal virtual RoomDTO ToDTO()
        {
            return new RoomDTO
            {
                RoomID = this.Id.ToString(),
                TrelloBoardID = this.TrelloBoardId,
                Date = this.Date,
                Link = this.Link
            };
        }

        public static string GenerateLink()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            return new String(stringChars);
        }
    }
}
