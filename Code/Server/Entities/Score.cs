﻿namespace Server.Entities
{
    public class Score: BaseEntity
    {
        public virtual User User { get; set; }
        public virtual Target Target { get; set; }
        public virtual float Value { get; set; }
    }
}
