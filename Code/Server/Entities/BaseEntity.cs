﻿using System;

namespace Server.Entities
{
    public abstract class BaseEntity
    {
        public virtual long Id { get; set; }

        public override bool Equals(Object other)
        {
            if ((other == null) || !this.GetType().Equals(other.GetType()))
            {
                return false;
            }
            else
            {
                BaseEntity otherBase = (BaseEntity) other;
                return (this.Id == otherBase.Id);
            }
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
