﻿namespace Server.Entities
{
    public class UserRoom : BaseEntity
    {
        public virtual User User { get; set; }
        public virtual Room Room { get; set; }
    }
}
