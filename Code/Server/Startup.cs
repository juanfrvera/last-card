using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Connection;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Type;
using NHibernate.Mapping.ByCode;
using Server.Entities;
using System.Linq;
using Server.DataComponents;
using System.IO;
using NHibernate.Context;
using Server.Services;
using Microsoft.OpenApi.Models;
using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;
using FirebaseAdmin.Auth;
using Server.Exceptions;
using System.Threading.Tasks;
using Server.Hubs;

namespace Server
{
    public class Startup
    {
        private const string HEADER_UID = "uid";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();
            services.AddSignalR();

            services.AddSingleton((provider) =>
            {
                var firebaseApp = FirebaseApp.Create(new AppOptions()
                {
                    Credential = GoogleCredential.FromFile("fbasepkey.json")
                });

                return firebaseApp;
            });

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { 
                    Title = "Last Card",
                    Version = "v1.2"
                });
                c.UseAllOfToExtendReferenceSchemas();
            });

            services.AddSingleton<RoomService>();
            services.AddSingleton<TargetService>();
            services.AddSingleton<UserService>();

            services.AddSingleton<ISessionFactory>((provider) =>
            {
                var cfg = new Configuration();
                string connStr = "server=" + Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.Configuration, "PostgresServer");
                connStr += ";port=" +        Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.Configuration, "PostgresPort");
                connStr += ";database=" +    Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.Configuration, "PostgresDatabase");
                connStr += ";user id=" +     Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.Configuration, "PostgresUser");
                connStr += ";password=" +    Microsoft.Extensions.Configuration.ConfigurationExtensions.GetConnectionString(this.Configuration, "PostgresPassword");

                cfg.DataBaseIntegration(config =>
                {
                    config.ConnectionProvider<DriverConnectionProvider>();
                    config.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;
                    config.Timeout = 60;
                    config.LogFormattedSql = true;
                    config.LogSqlInConsole = false;
                    config.AutoCommentSql = false;
                    config.Dialect<PostgreSQLDialect>();
                    config.Driver<NpgsqlDriver>();
                    config.ConnectionString = connStr;
                    config.SchemaAction = SchemaAutoAction.Recreate; // Recreate for testing, update for production.
                });

                cfg.SetNamingStrategy(new PostgreSQLNamingStrategy());

                ConventionModelMapper conventionalMappings = CreateConventionalMappings();
                var mapping = conventionalMappings.CompileMappingFor(GetType().Assembly.GetExportedTypes().Where(t => t.Namespace.EndsWith("Entities")));
                cfg.AddDeserializedMapping(mapping, "lastcard");

                cfg.CurrentSessionContext<AsyncLocalSessionContext>();

                var sessionFactory = cfg.BuildSessionFactory();

                // Itialize the allowed result types
                using (var session = sessionFactory.OpenSession())
                {
                    using (var transaction = session.BeginTransaction())
                    {
                        string[] types = Enum.GetNames(typeof(ResultType));
                        foreach(string str in types)
                        {
                            if (session.Query<AllowedResultType>().Where(c => c.Name.ToString() == str).Count() == 0)
                            {
                                var art = new AllowedResultType { Name = (ResultType) Enum.Parse(typeof(ResultType), str, false) };
                                session.Save(art);
                            }
                        }

                        transaction.Commit();
                    };

                    session.Close();
                }
                

                return sessionFactory;
            });

            services.AddScoped<NHibernate.ISession>((provider) =>
            {
                var factory = provider.GetService<ISessionFactory>();

                if (!CurrentSessionContext.HasBind(factory))
                    CurrentSessionContext.Bind(factory.OpenSession());

                return factory.GetCurrentSession();
            });
        }

        private ConventionModelMapper CreateConventionalMappings()
        {
            var mapper = new ConventionModelMapper();

            var baseEntityType = typeof(BaseEntity);
            mapper.IsEntity((t, declared) => baseEntityType.IsAssignableFrom(t) && baseEntityType != t && !t.IsInterface);
            mapper.IsRootEntity((t, declared) => baseEntityType.Equals(t.BaseType));

            mapper.BeforeMapManyToOne += (insp, prop, map) => map.Column(prop.LocalMember.GetPropertyOrFieldType().Name + "Id");
            mapper.BeforeMapManyToOne += (insp, prop, map) => map.Cascade(Cascade.Persist);
            mapper.BeforeMapBag += (insp, prop, map) => map.Key(km => km.Column(prop.GetContainerEntity(insp).Name + "Id"));
            mapper.BeforeMapBag += (insp, prop, map) => map.Cascade(Cascade.Persist);

            mapper.BeforeMapClass += (modelInspector, type, map) =>
            {
                map.Table($"{type.Name}s");
                map.Id(m =>
                {
                    m.Column($"{type.Name}ID");
                    m.Generator(Generators.Increment);
                });
            };

            mapper.Class<AllowedResultType>(map => map.Property(x => x.Name, attr => attr.Type<EnumStringType<ResultType>>()));

            mapper.Class<User>(map => map.Component<Avatar>(x => x.Avatar, cm =>
            { cm.Property(p => p.Picture, mc => mc.Column("Avatar")); }));

            mapper.Class<UserRoom>(map => map.ManyToOne(x => x.Room, m =>
            {
                m.Cascade(Cascade.Remove);
                m.NotNullable(false);
            }));

            mapper.Class<Room>(map => map.ManyToOne<User>(x => x.Host, cm => cm.Column("HostId")));
            mapper.Class<Room>(map => map.ManyToOne<Target>(x => x.CurrentTarget, cm => cm.Column("CurrentTargetID")));

            return mapper;
        }
        private async Task HandleException(int httpCode, HttpResponse response, Exception ex)
        {
            string message = ex.Message;
            if (httpCode == 500) // Hide the original exception to the client to avoid exposing internal information
                message = "The server encounter an internal error. Please contact the developers to fix the bug!";

            response.ContentType = "application/json";
            response.StatusCode = httpCode switch
            {
                401 => StatusCodes.Status401Unauthorized,
                403 => StatusCodes.Status403Forbidden,
                404 => StatusCodes.Status404NotFound,
                _ => StatusCodes.Status500InternalServerError, // Unknown error -> internal server exception
            };
                
            using var writer = new StreamWriter(response.Body);
            writer.Write(message);
            await writer.FlushAsync().ConfigureAwait(false);

            return;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
                 builder.WithOrigins("http://localhost:8100")
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
            );

            // Enable Swagger
            app.UseSwagger();

            // Enable Swagger UI
            app.UseSwaggerUI(c =>
            {
               c.SwaggerEndpoint("/swagger/v1/swagger.json", "Last Card API v1.2");
            });

            // Enable logs for incoming requests and response codes.
            app.UseMiddleware<RequestLoggingMiddleware>();

            // Error handling
            app.Use(async (context, next) =>
            {
                try
                {
                    await next.Invoke();
                }
                catch(UnauthorizedException ex)
                {
                    await HandleException(401, context.Response, ex);
                }
                catch(ForbiddenException ex)
                {
                    await HandleException(403, context.Response, ex);
                }
                catch(NotFoundException ex)
                {
                    await HandleException(404, context.Response, ex);
                }
                catch(Exception ex)
                {
                    await HandleException(-1, context.Response, ex);
                }
            });

            // Auth
            app.Use(async (context, next) =>
            {
                var firebaseApp = app.ApplicationServices.GetService<FirebaseApp>();
                var firebaseAuth = FirebaseAuth.GetAuth(firebaseApp);

#if DEBUG
                if (!context.Request.Headers.ContainsKey(HEADER_UID))
                {
                    // A valid UID for testing purposes
                    context.Request.Headers.Add("uid", "rBH3P95VMVhIH81ftHX2KKuO4fv1");
                }
#endif

                if (context.Request.Headers.ContainsKey(HEADER_UID))
                {
                    try
                    {
                        var uid = (context.Request.Headers[HEADER_UID]).First();
                        await firebaseAuth.GetUserAsync(uid);
                        context.Items.Add("uid", uid);

                        await next.Invoke();
                    }
                    catch(FirebaseAuthException authEx)
                    {
                        throw new UnauthorizedException(authEx);
                    }
                }
                else
                {
                        throw new UnauthorizedException();
                }
                        
            });

            // Creates a transaction for every request. Commit or Rollback at the end.
            app.UseMiddleware<SessionTransactionPerRequestMiddleware>();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<SignalRHub>("/signalr");
            });
        }
    }
}
