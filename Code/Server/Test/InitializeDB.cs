﻿using Server.Services;

namespace Server.Test
{
    public class InitializeDB
    {
        private readonly RoomService _roomService;
        private readonly TargetService _targetService;

        public InitializeDB(RoomService roomService, TargetService targetService)
        {
            _roomService = roomService;
            _targetService = targetService;
        }

        public void Init()
        {
            // old Test code need a review...

            /*
            RoomCreationDTO roomDTO = new RoomCreationDTO();
            roomDTO.User.Name = "Fabio";
            await _roomService.CreateRoom(roomDTO);

            roomDTO.User.Name = "Victoria";
            await _roomService.CreateRoom(roomDTO);

            roomDTO.User.Name = "Gabriel";
            RoomAndUserDTO roomUser = await _roomService.CreateRoom(roomDTO);


            TargetDTO targetDTO = new TargetDTO
            {
                Title = "target1",
                Description = "Target without description"
            };
            await _targetService.CreateTarget(roomUser.Room.Link, targetDTO);

            targetDTO = new TargetDTO
            {
                Title = "target2",
                Description = "A description"
            };
            await _targetService.CreateTarget(roomUser.Room.Link, targetDTO);

            targetDTO = new TargetDTO
            {
                Title = "target3",
                Description = "Another description"
            };
            await _targetService.CreateTarget(roomUser.Room.Link, targetDTO);

            targetDTO = new TargetDTO
            {
                Title = "target4",
                Description = "The last description"
            };
            await _targetService.CreateTarget(roomUser.Room.Link, targetDTO);
            */
        }
    }

}
