-- Role: lastcard_server
CREATE ROLE lastcard_server WITH
  LOGIN
  NOSUPERUSER
  INHERIT
  NOCREATEDB
  NOCREATEROLE
  NOREPLICATION
  ENCRYPTED PASSWORD 'SCRAM-SHA-256$4096:vrJHBLYyYFx/Dcm2rTCMfw==$Xw8N/Vl2RUjMiMQBvJO3rgBCYqEd5xtAt25hhFwnn84=:QmgM8xLJdzA3mem1nncOMxIjLABVRqFTGuKiXQ84vRs=';

-- Database: lastcard
CREATE DATABASE lastcard
    WITH 
    OWNER = lastcard_server
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

-- Some example Data	
/*
INSERT INTO public."Users"("UserID", "Name", "Avatar", "TrelloUserId") VALUES (1, "Victoria", "Default", null);
INSERT INTO public."Users"("UserID", "Name", "Avatar", "TrelloUserId") VALUES (2, "Carolina", "Default", null);
INSERT INTO public."Users"("UserID", "Name", "Avatar", "TrelloUserId") VALUES (3, "Milena", "Default", null);
INSERT INTO public."Users"("UserID", "Name", "Avatar", "TrelloUserId") VALUES (4, "Lucy", "Default", null);
INSERT INTO public."Rooms"("RoomID", "Date", "Link", "TrelloBoardId", "HostId") VALUES (1, '2020-10-23 19:14:24.885365', "6IeR0i3K", null, 3);
INSERT INTO public."Rooms"("RoomID", "Date", "Link", "TrelloBoardId", "HostId") VALUES (2, '2020-10-26 12:09:20.763757', "WH3xCLjB", null, 1);
INSERT INTO public."Rooms"("RoomID", "Date", "Link", "TrelloBoardId", "HostId") VALUES (3, '2020-10-30 14:12:31.072602', "lexzfLPR", null, 4);
INSERT INTO public."Targets"("TargetID", "Title", "Description", "ResultId", "TrelloTargetId", "AllowedResultTypeId", "RoomId") VALUES (1, "testTarget1", "No description", null, null, "Numeric", 1);
INSERT INTO public."Targets"("TargetID", "Title", "Description", "ResultId", "TrelloTargetId", "AllowedResultTypeId", "RoomId") VALUES (2, "testTarget2", "A description", null, null, "Numeric", 1);
INSERT INTO public."Targets"("TargetID", "Title", "Description", "ResultId", "TrelloTargetId", "AllowedResultTypeId", "RoomId") VALUES (3, "testTarget3", "Other description", null, null, "Numeric", 1);
INSERT INTO public."Targets"("TargetID", "Title", "Description", "ResultId", "TrelloTargetId", "AllowedResultTypeId", "RoomId") VALUES (4, "testTarget4", "No description", null, null, "Numeric", 2);
*/