﻿namespace Server.DataComponents
{
    public enum ResultType
    {
        Numeric,
        Alphabetic,
        Character
    }
}
