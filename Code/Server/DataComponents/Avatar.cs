﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.DataComponents
{
    public class Avatar
    {
        public string Picture { get; set; }

        public static Avatar DefaultAvatar()
        {
            return new Avatar { Picture = "Default" };
        }
    }
}
