export interface IRoomData {
    link: string;
    trelloBoardID?: string;
}