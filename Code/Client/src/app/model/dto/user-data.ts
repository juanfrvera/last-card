export interface IUserData {
    id: string;
    name: string;
    avatar: string;
}
