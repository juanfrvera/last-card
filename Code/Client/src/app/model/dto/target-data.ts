export interface ITargetData {
    id : string;
    title : string;
    description? : string;
    finalResult? : number;
}
