export interface IUserCreationData {
    avatar : string;
    name : string;
}
