import { IRoomData } from "./room-data";
import { IUserData } from "./user-data";

/**Used when joining a room, to return the joined room data and the user data */
export interface IRoomAndUserData {
    room: IRoomData;
    user: IUserData;
}
