import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { link } from "fs";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable()
export class AppInterceptor implements HttpInterceptor {
    private static _roomLink: string;
    private static _signalRID: string;
    private static _uid: string;

    public static set roomLink(value: string) {
        this._roomLink = value;
    }


    public static get uid(): string {
        return this._uid;
    }
    public static set uid(value: string) {
        this._uid = value;
    }

    public static get signalRID(): string {
        return this._signalRID;
    }
    public static set signalRID(value: string) {
        console.log("signalR ID set");

        this._signalRID = value;

        if (this.onSignalRIDSet == null) {
            this.onSignalRIDSet = new BehaviorSubject(null);
        }
        this.onSignalRIDSet.next(null);
    }
    public static onSignalRIDSet: BehaviorSubject<any> = new BehaviorSubject(null);


    // Intercept all http requests to add the uid header
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let cloneHeaders: HttpHeaders = req.headers;

        if (AppInterceptor._roomLink != null) cloneHeaders = cloneHeaders.set('link', AppInterceptor._roomLink);
        if (AppInterceptor._uid != null) cloneHeaders = cloneHeaders.set('uid', AppInterceptor._uid);
        if (AppInterceptor._signalRID != null) cloneHeaders = cloneHeaders.set('signalRId', AppInterceptor._signalRID);



        // Clone the request to add the headers
        const clonedRequest = req.clone({
            headers: cloneHeaders
        });

        // Pass the cloned request instead of the original request to the next handle
        return next.handle(clonedRequest);
    }
}