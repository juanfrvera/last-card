import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { AlertService } from '../../services/alert.service';
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-join-options',
  templateUrl: './join-options.page.html',
  styleUrls: ['./join-options.page.scss'],
})
export class JoinOptionsPage implements OnInit {

  public link: string;
  public loading: boolean;

  constructor(
    private alert: AlertService,
    private app: AppService,
    private barcodeScanner: BarcodeScanner,
    private router: Router
  ) { }

  ngOnInit() {
  }

  joinWithLink(link: string) {
    this.loading = true;
    this.app.isRoomJoinable(this.link).then(canJoin => {
      if (canJoin) {
        this.router.navigate(["join", link]);
      }
      else {
        this.alert.errorToast("The room is not joinable");
      }
      this.loading = false;
    },
      (error) => {
        this.alert.errorToast("An error has occured or the room doesn't exists");
        this.loading = false;
      });
  }

  joinWithQR() {
    this.barcodeScanner.scan({ formats: "QR_CODE", prompt: "Scan QR code" } as BarcodeScannerOptions).then(barcodeData => {
      this.joinWithLink(barcodeData.text);

    }).catch(err => {
      this.alert.errorToast("An error has occured while scanning QR code");
    });
  }

}
