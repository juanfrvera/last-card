import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JoinOptionsPage } from './join-options.page';

describe('JoinOptionsPage', () => {
  let component: JoinOptionsPage;
  let fixture: ComponentFixture<JoinOptionsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinOptionsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JoinOptionsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
