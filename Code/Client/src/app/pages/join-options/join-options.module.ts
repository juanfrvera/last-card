import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JoinOptionsPageRoutingModule } from './join-options-routing.module';

import { JoinOptionsPage } from './join-options.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JoinOptionsPageRoutingModule
  ],
  declarations: [JoinOptionsPage]
})
export class JoinOptionsPageModule {}
