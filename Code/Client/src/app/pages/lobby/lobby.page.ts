import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../services/server.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IRoomAndUserData } from '../../model/dto/room-and-user-data';
import { AppService } from '../../services/app.service';
import { AlertService } from '../../services/alert.service';
import { IUserCreationData } from '../../model/dto/user-creation-data';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.page.html',
  styleUrls: ['./lobby.page.scss'],
})
export class LobbyPage implements OnInit {
  /* --------- View variables --------- */
  // User name
  public user: IUserCreationData;

  // Used when waiting the server after an api request
  public loading: boolean = false;

  // Used to check what mode we are on (create or join)
  public mode: string;

  private link: string;

  constructor(
    private activatedroute: ActivatedRoute,
    private alert: AlertService,
    private app: AppService,
    private router: Router) { }

  ngOnInit() {
    this.activatedroute.paramMap.subscribe(params => {
      this.link = params.get('link');

      if (this.link != null) {
        this.mode = "join";
      }
      else {
        this.mode = "create";
      }
    });

    // Fallback
    this.user = { avatar: "Wolf", name: null };

    this.changeAvatar();
  }

  nameChanged() {
  }

  changeAvatar() {
    this.user.avatar = this.app.getRandomAvatar();
  }

  /**Continue to the room */
  continue() {
    if (this.user.name != null && this.user.name.length > 0) {
      this.loading = true;

      if (this.mode == "create") {
        this.app.createRoom(this.user).then((response) => {
          console.log("success, response: " + JSON.stringify(response));

          // Go to room with the room link on the URL
          this.router.navigate(['/' + response.room.link]);
        },
          (error) => {
            this.loading = false;
            console.log("error: " + error);
            this.alert.errorToast("Couldn't create room, try again in a moment");
          });
      }
      else {
        // Join
        if (this.link != null) {
          this.app.joinRoom(this.link, this.user).then(() => {
            this.router.navigate([this.link]);
          },
            (error) => {
              this.alert.errorToast("An error has ocurred when trying to join the room");
              this.loading = false;
            });
        }
        else {
          this.alert.errorToast("Invalid room link!");
        }
      }
    }
  }
}
