import { AlertService } from '../../services/alert.service';
import { AppService } from '../../services/app.service';
import { Component } from '@angular/core';
import { ItemReorderEventDetail } from '@ionic/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-room',
  templateUrl: './room.page.html',
  styleUrls: ['./room.page.scss'],
})
export class RoomPage {
  /* ----View variables ---- */
  public selectedTargetIndex?: number;

  get SelectedTarget() {
    if (this.selectedTargetIndex != null) {
      return this.Targets[this.selectedTargetIndex];
    }
    else {
      return null;
    }
  }

  public get Targets() {
    return this.app.Targets;
  }

  public get Users() {
    return this.app.Users;
  }

  public get HostId() {
    return this.app.HostId;
  }

  get IsHost() {
    return this.app.IsHost;
  }

  constructor(
    private alert: AlertService,
    private app: AppService,
    private router: Router) { }

  /**Redirects to "new target" page */
  newTarget() {
    this.router.navigate(['/' + this.app.Room.link + '/new']);
  }

  /**Redirects to "edit target" page */
  editTarget() {
    this.router.navigate(['/' + this.app.Room.link + '/edit/' + this.Targets[this.selectedTargetIndex].id]);
  }

  /**When the user wants to delete the selected target */
  deleteSelectedTarget() {
    // Make a copy of the current index in case it is changed somehow
    const selectedIndex = this.selectedTargetIndex;

    this.alert.confirmationDialog(
      'Delete',
      'Do you really want to delete the selected target?',
      // Called when the user accepts the deletion
      () => {
        // Delete on server
        this.app.deleteTarget(this.SelectedTarget.id).then(() => {
          // There is no more a selected target
          this.selectedTargetIndex = undefined;
        },
          (error) => {
            this.alert.errorToast("Server error when deleting");
            console.log(error);
          });
      });
  }

  reorderTarget(ev: CustomEvent<ItemReorderEventDetail>) {
    // The `from` and `to` properties contain the index of the item
    // when the drag started and ended, respectively
    console.log('Dragged from index', ev.detail.from, 'to', ev.detail.to);

    // Finish the reorder and position the item in the DOM based on
    // where the gesture ended. This method can also be called directly
    // by the reorder group
    ev.detail.complete();
  }

  startRoundForSelectedTarget() {
    this.app.startRound(this.Targets[this.selectedTargetIndex]).catch(error => {
      this.alert.errorToast("Couldn't start round with selected target");
    });
  }

  targetSelected(index: number) {
    this.selectedTargetIndex = index;
  }

  targetStartSelecting(index: number) {
    if (index != this.selectedTargetIndex) {
      // User to clear the selected target in the view
      this.selectedTargetIndex = undefined;
    }
  }
}
