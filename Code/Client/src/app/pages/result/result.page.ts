import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  public get Result() : number{
    return this.app.CurrentTarget.finalResult;
  }

  get Users() {
    return this.app.Users;
  }

  get IsHost() {
    return this.app.IsHost;
  }

  constructor(
    private alert: AlertService,
    private app: AppService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  off: boolean = true;
  

  showResult() {
    this.app.showResult();
  }

  /**When the user wants to cancel the current round */
  finish() {
    this.app.exitRound().catch(error => {
      this.alert.errorToast("Server error when finishing the round");
    });
  }
}
