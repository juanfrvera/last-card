import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AppService } from '../../services/app.service';
import { ITargetData } from '../../model/dto/target-data';

@Component({
  selector: 'app-target-edition',
  templateUrl: './target-edition.page.html',
  styleUrls: ['./target-edition.page.scss'],
})
export class TargetEditionPage implements OnInit {
  public mode: string;
  public target: ITargetData;

  constructor(
    private activatedroute: ActivatedRoute,
    private app: AppService,
    private router: Router,
    private toastController: ToastController,
    private location: Location
  ) { }

  ngOnInit() {
    this.activatedroute.paramMap.subscribe(params => {
      const targetId: string = params.get('id');

      if (targetId != null) {
        this.target = this.app.getTarget(targetId);
        this.mode = "edit";
      }
      else {
        this.mode = "new";
        // Empty target
        this.target = {} as ITargetData;
      }
    });
  }

  /**Confirms the data entered, it distinguishes between a target creation or a target edition*/
  confirm() {
    if (this.target.title != null && this.target.title.length > 0) {
      if (this.mode == "edit") {
        this.app.editTarget(this.target).subscribe(() => {
          this.router.navigate(['/' + this.app.Room.link]);
        }, () => {
          this.errorToast("Try again in a moment");
        });
      }
      else {
        this.app.createTarget(this.target).then((response: ITargetData) => {
          console.log("created, response: " + JSON.stringify(response));
          this.router.navigate(['/' + this.app.Room.link]);
        },
          (error) => {
            console.log("error: " + error);
            this.errorToast("Try again in a moment");
          })
      }
    }
    else {
      this.errorToast("You must set a title");
    }
  }

  async errorToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  /**Returns to the previous page */
  goBack() {
    this.location.back();
  }
}
