import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TargetEditionPageRoutingModule } from './target-edition-routing.module';

import { TargetEditionPage } from './target-edition.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TargetEditionPageRoutingModule
  ],
  declarations: [TargetEditionPage]
})
export class TargetEditionPageModule {}
