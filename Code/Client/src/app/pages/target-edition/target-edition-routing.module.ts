import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TargetEditionPage } from './target-edition.page';

const routes: Routes = [
  {
    path: '',
    component: TargetEditionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TargetEditionPageRoutingModule {}
