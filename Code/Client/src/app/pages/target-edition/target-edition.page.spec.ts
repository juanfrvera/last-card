import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TargetEditionPage } from './target-edition.page';

describe('TargetEditionPage', () => {
  let component: TargetEditionPage;
  let fixture: ComponentFixture<TargetEditionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetEditionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TargetEditionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
