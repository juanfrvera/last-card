import { Component, OnInit } from '@angular/core';
import { ITargetData } from '../../model/dto/target-data';
import { AlertService } from '../../services/alert.service';
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-round',
  templateUrl: './round.page.html',
  styleUrls: ['./round.page.scss'],
})
export class RoundPage implements OnInit {

  public target: ITargetData;

  public get Users() {
    return this.app.Users;
  }

  public get HostId() {
    return this.app.HostId;
  }

  get IsHost() {
    return this.app.IsHost;
  }

  constructor(
    private alert: AlertService,
    private app: AppService) { }

  ngOnInit() {
    this.target = this.app.CurrentTarget;
  }

  /**When the user wants to cancel */
  wantsToCancel() {
    this.alert.confirmationDialog(
      'Cancel Round',
      'Do you really want to cancel the current round?',
      // Called when the user accepts the deletion
      () => {
        // Delete on server
        this.app.exitRound().catch(error => {
          this.alert.errorToast("Server error when canceling the round");
        });
      });
  }

  confirmRound() {
    this.app.confirmRound().catch(error => {
      this.alert.errorToast("Server error when confirming the round");
    });
  }
}
