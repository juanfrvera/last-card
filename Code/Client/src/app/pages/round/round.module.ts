import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoundPageRoutingModule } from './round-routing.module';

import { RoundPage } from './round.page';
import { ScoringComponent } from '../../components/scoring/scoring.component';
import { SharedModule } from '../../modules/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoundPageRoutingModule,
    SharedModule
  ],
  declarations: [RoundPage, ScoringComponent]
})
export class RoundPageModule {}
