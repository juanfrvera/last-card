import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'join-options',
    loadChildren: () => import('./pages/join-options/join-options.module').then( m => m.JoinOptionsPageModule)
  },
  {
    path: 'join/:link',
    loadChildren: () => import('./pages/lobby/lobby.module').then(m => m.LobbyPageModule)
  },
  {
    path: 'create',
    loadChildren: () => import('./pages/lobby/lobby.module').then(m => m.LobbyPageModule)
  },
  {
    path: ':link/edit/:id',
    loadChildren: () => import('./pages/target-edition/target-edition.module').then( m => m.TargetEditionPageModule),
  },
  {
    path: ':link/new',
    loadChildren: () => import('./pages/target-edition/target-edition.module').then( m => m.TargetEditionPageModule),
  },
  {
    path: ':link/result',
    loadChildren: () => import('./pages/result/result.module').then( m => m.ResultPageModule)
  },
  {
    path: ':link',
    loadChildren: () => import('./pages/room/room.module').then(m => m.RoomPageModule)
  },
  {
    path: ':link/round',
    loadChildren: () => import('./pages/round/round.module').then( m => m.RoundPageModule)
  },

  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
