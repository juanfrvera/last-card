import { Injectable } from '@angular/core';
import * as signalR from "@aspnet/signalr";
import { ServerService } from './server.service';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {

  public hubConnection: signalR.HubConnection;

  private _connected: boolean = false;

  public get connected(): boolean {
    return this._connected;
  }

  public startConnection(): Promise<string> {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(ServerService.api + "signalr")
      .build();

    return this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started');
        this._connected = true;
        return this.hubConnection.invoke('GetConnectionId');
      })
      .catch(err => console.log('Error while starting signalR connection: ' + err));
  }
}