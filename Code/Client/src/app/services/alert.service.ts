import { AlertController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    private alertController: AlertController,
    private toastController: ToastController) { }

  /**
   * asnyc confirmationDialog
   */
  public async confirmationDialog(
    header : string,
    message: string,
    onOk?: (value: any) => any,
    onCancel?: (value: any) => any) {

    const alert = await this.alertController.create({
      cssClass: 'confirmation-dialog',
      header: header,
      //subHeader: 'Subtitle',
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: onOk
        },
        {
          text: 'CANCEL',
          role: 'cancel',
          cssClass: 'secondary',
          handler: onCancel
        }
      ]
    });

    await alert.present();
  }

  public async errorToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
}
