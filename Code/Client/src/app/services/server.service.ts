import { DummyService } from './dummy.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IRoomAndUserData } from '../model/dto/room-and-user-data';
import { ITargetData } from '../model/dto/target-data';
import { IUserCreationData } from '../model/dto/user-creation-data';
import { IUserData } from '../model/dto/user-data';
import { IScore } from '../model/dto/score';

@Injectable({
  providedIn: 'root'
})
export class ServerService {

  // Used for testing
  public static readonly dummyMode: boolean = false;

  private readonly errorPercentage = 0.25;
  private readonly testTimeMs = 3000;

  public static readonly api = "http://localhost:52802/";

  constructor(private http: HttpClient) { }

  //#region User
  public getRandomAvatar() {
    // TODO: Take it out of dummy service
    return DummyService.getRandomAvatar();
  }
  //#endregion
  //#region Room
  /**Creates the room and returns the room created room's link */
  public createRoom(creationData: IUserCreationData): Observable<IRoomAndUserData> {
    if (ServerService.dummyMode) {

      const dummyResponse: IRoomAndUserData = {
        room: DummyService.emptyRoom,
        user: {
          id: '1',
          avatar: creationData.avatar,
          name: creationData.name
        }
      };

      return this.dummyObservable(dummyResponse);
    }
    else {
      return this.http.post<IRoomAndUserData>(ServerService.api + "room", creationData);
    }
  }
  public isRoomJoinable(): Observable<boolean> {
    if (ServerService.dummyMode) {
      return this.dummyObservable(true);
    }
    else {
      return this.http.get<boolean>(ServerService.api + "room");
    }
  }
  public joinRoom(link: string, user: IUserCreationData): Observable<IRoomAndUserData> {
    if (ServerService.dummyMode) {
      DummyService.filledRoom.link = link;

      const roomAndUser: IRoomAndUserData = {
        room: DummyService.filledRoom,
        user: {
          id: '99',
          avatar: user.avatar,
          name: user.name
        }
      }

      return this.dummyObservable(roomAndUser);
    }
    else {
      return this.http.put<IRoomAndUserData>(ServerService.api + "room", user);
    }
  }
  //#endregion
  //#region  Round
  /**
 *  Starts a new round for the target
 */
  public startRound(targetId: string) {
    if (ServerService.dummyMode) {
      return this.dummyObservable(true);
    }
    else {
      return this.http.post(ServerService.api + "target/start", { ID: targetId });
    }
  }
  /**Cancels the current round */
  public exitRound() {
    if (ServerService.dummyMode) {
      return this.dummyObservable(true);
    }
    else {
      return this.http.post<boolean>(ServerService.api + "target/exit", {});
    }
  }
  public confirmRound() {
    if (ServerService.dummyMode) {
      return this.dummyObservable(true);
    }
    else {
      return this.http.post<boolean>(ServerService.api + "target/finish", {});
    }
  }
  public showResult() : Observable<number>{
    if (ServerService.dummyMode) {
      return this.dummyObservable(10);
    }
    else {
      return this.http.post<number>(ServerService.api + "target/showresult", {});
    }
  }

  /**Gets the id of the target that is being scored */
  public getCurrentTargetId(): Observable<string> {
    if (ServerService.dummyMode) {
      return this.dummyObservable(null);
    }
    else {
      return this.http.get<string>(ServerService.api + "target/current");
    }
  }

  public sendCurrentScore(score: IScore) {
    if (ServerService.dummyMode)
      return this.dummyObservable(true);
    else
      return this.http.post(ServerService.api + "target/score", score);
  }

  //#endregion
  //#region Target
  public createTarget(target: ITargetData): Observable<ITargetData> {
    if (ServerService.dummyMode) {
      target.id = DummyService.newTargetId();

      const obs = this.dummyObservable(target);

      obs.subscribe(() => {
        // Claim the ID so newer targets doesn't use it
        DummyService.claimNewTargetId();
      })

      return obs;
    }
    else {
      return this.http.post<ITargetData>(ServerService.api + "target", target);
    }
  }

  public getTargets() {
    if (ServerService.dummyMode) {
      return this.dummyObservable(DummyService.targets);
    }
    else {
      return this.http.get<ITargetData[]>(ServerService.api + "target");
    }
  }

  public editTarget(target: ITargetData) {
    if (ServerService.dummyMode)
      return this.dummyObservable(true);
    else
      return this.http.put<boolean>(ServerService.api + "target/" + target.id, target);
  }

  public deleteTarget(targetId: string) {
    if (ServerService.dummyMode) {
      return this.dummyObservable(true);
    }
    else {
      return this.http.delete<boolean>(ServerService.api + "target/" + targetId);
    }
  }
  //#endregion
  //#region  Users
  public getUsers(): Observable<IUserData[]> {
    if (ServerService.dummyMode) {
      return this.dummyObservable(DummyService.users);
    }
    else {
      return this.http.get<IUserData[]>(ServerService.api + "room/user");
    }
  }
  public getHostId(): Observable<string> {
    if (ServerService.dummyMode) {
      return this.dummyObservable('1');
    }
    else {
      return this.http.get<string>(ServerService.api + "room/host");
    }
  }
  //#endregion Users

  /**Returns an observable with a probability of error */
  private dummyObservable(value: any): Observable<any> {
    const rnd = Math.random();

    return new Observable(observer => {
      // Delay the observable some seconds for test purposes
      setTimeout(() => {

        // Error probability, for test purposes
        if (rnd < this.errorPercentage) {
          observer.error(new Error("Test error"));
        }
        else {
          observer.next(value);
        }

        observer.complete();
      }, rnd * this.testTimeMs);
    });
  }
}
