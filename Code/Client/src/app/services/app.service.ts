import { DummyService } from './dummy.service';
import { Injectable } from '@angular/core';
import { IRoomData } from '../model/dto/room-data';
import { ServerService } from './server.service';
import { ITargetData } from '../model/dto/target-data';
import { IUserData } from '../model/dto/user-data';
import { IUserCreationData } from '../model/dto/user-creation-data';
import { AngularFireAuth } from '@angular/fire/auth';
import { BehaviorSubject } from 'rxjs';
import { AppInterceptor } from '../app.interceptor';
import { IRoomAndUserData } from '../model/dto/room-and-user-data';
import { SignalRService } from './signal-r.service';
import { Router } from '@angular/router';
import { IScore } from '../model/dto/score';

@Injectable({
  providedIn: 'root'
})

/**Holds client data */
export class AppService {
  private currentTarget: ITargetData;
  /**Current room */
  private room: IRoomData;
  /**Current user */
  private user: IUserData;

  private users: IUserData[] = [];
  private targets: ITargetData[];

  private onUidSet: BehaviorSubject<any> = new BehaviorSubject(null);

  private hostId: string;

  public get CurrentTarget(): ITargetData {
    return this.currentTarget;
  }

  public get IsHost(): boolean {
    return this.User.id == this.HostId;
  }

  public get HostId(): string {
    return this.hostId;
  }

  /**Current room */
  public get Room(): IRoomData {
    return this.room;
  }
  /**Current room */
  public set Room(room: IRoomData) {
    this.room = room;

    AppInterceptor.roomLink = room.link;
  }

  /**List of targets of the current room */
  public get Targets(): ITargetData[] {
    return this.targets;
  }

  /**Current user */
  public get User(): IUserData {
    return this.user;
  };
  /**Current user */
  public set User(user: IUserData) {
    this.user = user;
  }

  /**List of users in the current room */
  public get Users(): IUserData[] {
    return this.users;
  }

  constructor(private fireauth: AngularFireAuth, private router: Router, private server: ServerService, private signalr: SignalRService) {

    // Used for test purposes
    if (ServerService.dummyMode) {
      this.Room = DummyService.filledRoom;
      this.targets = DummyService.targets;
      this.users = DummyService.users;
      this.hostId = '1';
    }

    this.fireauth.onAuthStateChanged(this.authStateChanged);
  }

  authStateChanged(user: firebase.default.User) {
    if (user) {
      AppInterceptor.uid = user.uid;
      // This can be null when the auth state changes on constructor
      if (this.onUidSet == null) {
        this.onUidSet = new BehaviorSubject(null);
      }
      this.onUidSet.next(null);
      console.log("User id: " + user.uid);
      // ...
    } else {
      // User is signed out.
      // ...
    }
  }

  //#region User
  public getRandomAvatar() {
    return this.server.getRandomAvatar();
  }
  //#endregion
  //#region Room
  public createRoom(creationData: IUserCreationData) {
    return this.initializeConnection().then(() => {
      return this.server.createRoom(creationData).toPromise().then((response) => {
        this.afterRoomCreated(response);
        return response;
      });
    });
  }
  public isRoomJoinable(link: string) {
    AppInterceptor.roomLink = link;
    // For now every user can join the room if it exists.
    // TODO: add join restriction behavior

    if (AppInterceptor.uid != null) {
      return this.server.isRoomJoinable().toPromise().then(() => true, () => false);
    }
    else {
      return this.signInAnonymously().then(response => {
        AppInterceptor.uid = (response as firebase.default.auth.UserCredential).user.uid;
        return this.server.isRoomJoinable().toPromise().then(() => true, () => false);
      });
    }
  }

  public joinRoom(link: string, user: IUserCreationData) {
    return this.initializeConnection().then(() => {
      return this.server.joinRoom(link, user).toPromise().then(response => {
        return this.afterRoomJoin(response);
      });
    });
  }
  //#endregion

  //#region  Target
  public createTarget(target: ITargetData) {
    return this.server.createTarget(target).toPromise().then((response) => {
      this.Targets.push(response);

      return response;
    });
  }

  public getTarget(targetId: string) {
    return this.Targets.find(t => t.id == targetId);
  }

  public editTarget(target: ITargetData) {
    return this.server.editTarget(target);
  }

  public deleteTarget(targetId: string) {
    return this.server.deleteTarget(targetId).toPromise().then(() => {
      this.deleteTargetLocally(targetId);
    })
  }
  //#endregion

  //#region  Round
  /**
   * Informs the server that we want to start a round for the target
   * and saves the current target for future usage
   */
  public startRound(target: ITargetData) {
    return this.server.startRound(target.id).toPromise().then(response => {
      this.goToRound(target);
    });
  }
  public exitRound() {
    return this.server.exitRound().toPromise().then(() => {
      this.goToRoom();
    });
  }
  public confirmRound() {
    return this.server.confirmRound().toPromise().then(() => {
      this.goToResult();
    });
  }
  public showResult() {
    return this.server.showResult().toPromise().then((result) => {
      this.CurrentTarget.finalResult = result;
    });
  }
  public sendCurrentScore(score: IScore) {
    return this.server.sendCurrentScore(score).toPromise().then(() => {

    });
  }
  //#endregion
  //#region  Local modifications
  /**Redirects the client to the round view with the corresponding target */
  public goToRound(target: ITargetData) {
    this.currentTarget = target;
    this.router.navigate(['/' + this.Room.link + '/round']);
  }
  public goToResult() {
    this.router.navigate(['/' + this.Room.link + '/result']);
  }
  public goToRoom() {
    this.router.navigate(['/' + this.Room.link]);
  }
  public goToHome() {
    this.router.navigate(['']);
  }
  private deleteTargetLocally(targetId: string) {
    const index = this.targets.findIndex(t => t.id == targetId);
    this.Targets.splice(index, 1);
  }
  private deleteUserLocally(userId: string) {
    const index = this.Users.findIndex(u => u.id == userId);
    this.Users.splice(index, 1);
  }
  //#endregion
  //#region SignalR
  private startListeningSignalR() {
    this.signalr.hubConnection.on("newuser", newUser => this.signalRNewUser(newUser));
    this.signalr.hubConnection.on("newtarget", newTarget => this.signalRNewTarget(newTarget));
    this.signalr.hubConnection.on("targetedit", editedTarget => this.signalRTargetEdit(editedTarget));
    this.signalr.hubConnection.on("targetdelete", targetId => this.signalRTargetDelete(targetId));
    this.signalr.hubConnection.on("startround", targetId => this.signalRStartRound(targetId));
    this.signalr.hubConnection.on("finishround", () => this.signalRFinishRound());
    this.signalr.hubConnection.on("showresult", result => this.signalRShowResult(result));
    this.signalr.hubConnection.on("exitround", () => this.signalRExitRound());
    this.signalr.hubConnection.on("userdelete", userId => this.signalRUserDelete(userId));
  }
  private signalRNewUser(user: IUserData) {
    this.users.push(user);
  }
  private signalRNewTarget(target: ITargetData) {
    this.targets.push(target);
  }
  private signalRTargetEdit(target: ITargetData) {
    let localTarget = this.targets.find(t => t.id == target.id);

    localTarget.title = target.title;
    localTarget.description = target.description;
  }
  private signalRTargetDelete(targetId: string) {
    this.deleteTargetLocally(targetId);
  }
  private signalRStartRound(targetId: string) {
    this.goToRound(this.targets.find(t => t.id == targetId));
  }
  private signalRExitRound() {
    this.goToRoom();
  }
  private signalRFinishRound() {
    this.goToResult();
  }
  private signalRShowResult(result: number) {
    console.log("Result received: " + result);
    // This should also edit the instance that is in the targets list
    this.CurrentTarget.finalResult = result;
  }
  private signalRUserDelete(userId: string) {
    if (userId != this.HostId) {
      // Only delete if there is a user with this Id
      if (this.Users.some(u => u.id == userId)) {
        this.deleteUserLocally(userId);
      }
    }
    else {
      this.goToHome();
    }
  }
  //#endregion
  private afterRoomCreated(data: IRoomAndUserData) {
    this.Room = data.room;
    this.User = data.user;
    this.users = [this.User];
    this.hostId = data.user.id;
    this.targets = [];
  }
  private afterRoomJoin(data: IRoomAndUserData) {
    this.Room = data.room;
    this.User = data.user;

    return this.server.getUsers().toPromise().then(users => {
      this.users = users;

      return this.server.getHostId().toPromise().then(hostId => {
        this.hostId = hostId;

        return this.server.getTargets().toPromise().then(targets => {
          this.targets = targets;

          return this.server.getCurrentTargetId().subscribe(targetId => {
            // Go to round if there is one target being scored
            if (targetId != null && targetId != "") {
              this.goToRound(this.targets.find(t => t.id == targetId));
            }
          })
        });
      });
    });
  }
  private initializeConnection() {
    let firebasePromise: Promise<any> = null;
    let signalRPromise: Promise<any> = null;

    if (AppInterceptor.uid == null) {
      firebasePromise = this.signInAnonymously();

      firebasePromise.then(response => {
        AppInterceptor.uid = (response as firebase.default.auth.UserCredential).user.uid;
      });
    }
    if (!this.signalr.connected) {
      signalRPromise = this.signalr.startConnection();

      signalRPromise.then(connectionID => {
        AppInterceptor.signalRID = connectionID;
        this.startListeningSignalR();
      });
    }

    return Promise.all([firebasePromise, signalRPromise]);
  }
  private signInAnonymously() {
    return this.fireauth.signInAnonymously().catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
      console.log("Error: " + errorCode + ". Message: " + errorMessage);
    });
  }
}
