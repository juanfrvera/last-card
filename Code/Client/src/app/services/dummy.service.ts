import { Injectable } from '@angular/core';
import { IRoomData } from '../model/dto/room-data';
import { ITargetData } from '../model/dto/target-data';
import { IUserData } from '../model/dto/user-data';

@Injectable({
  providedIn: 'root'
})
export class DummyService {

  public static readonly emptyRoom: IRoomData = {
    link: 'kJq42F',
  }

  public static readonly targets: ITargetData[] = [
    { id: '1', title: 'target1', description: 'description 1' },
    { id: '2', title: 'target2', description: 'description 2, a little larger than the previous' },
    { id: '3', title: 'target3', description: 'This is a description' },
    { id: '4', title: 'Random target title', description: 'Randomized description' },
    { id: '5', title: 'Target with no description' },
    { id: '6', title: 'Do this thing', description: 'Hence the thing' },
    { id: '7', title: 'Do that other thing', description: 'Do this, do that, do another' },
    { id: '8', title: 'Do it', description: 'description 8, yes' }
  ];

  public static readonly users: IUserData[] = [
    { id: '1', name: 'random name 1', avatar: "Badger" },
    { id: '2', name: 'peDrito clApier', avatar: "Chupacabra" },
    { id: '3', name: 'Enzo Gomez Rodriguez', avatar: "Giraffe" },
    { id: '4', name: 'user4', avatar: "Turtle" },
    { id: '4', name: 'user5', avatar: "Dingo" },
    { id: '4', name: 'Jerusalem Endesite Ficticio', avatar: "Fox" },
    { id: '4', name: 'user7', avatar: "Duck" }
  ];

  public static readonly filledRoom: IRoomData = {
    link: 'hjxS2'
  }

  public static newTargetId(): string {
    return (this.lastTargetId + 1).toString();
  }

  public static claimNewTargetId() {
    this.lastTargetId++;
  }

  public static getRandomAvatar(): string {
    const rnd = this.getRandomInt(0, this.anonymousAnimals.length - 1);
    return this.anonymousAnimals[rnd];
  }

  private static anonymousAnimals: string[] = [
    "Alligator",
    "Anteater",
    "Armadillo",
    "Auroch",
    "Axolotl",
    "Badger",
    "Bat",
    "Beaver",
    "Buffalo",
    "Camel",
    "Capybara",
    "Chameleon",
    "Cheetah",
    "Chinchilla",
    "Chipmunk",
    "Chupacabra",
    "Cormorant",
    "Coyote",
    "Crow",
    "Dingo",
    "Dinosaur",
    "Dolphin",
    "Duck",
    "Elephant",
    "Ferret",
    "Fox",
    "Frog",
    "Giraffe",
    "Gopher",
    "Grizzly",
    "Hedgehog",
    "Hippo",
    "Hyena",
    "Ibex",
    "Ifrit",
    "Iguana",
    "Jackal",
    "Kangaroo",
    "Koala",
    "Kraken",
    "Lemur",
    "Leopard",
    "Liger",
    "Llama",
    "Manatee",
    "Mink",
    "Monkey",
    "Moose",
    "Narwhal",
    "Nyan Cat",
    "Orangutan",
    "Otter",
    "Panda",
    "Penguin",
    "Platypus",
    "Pumpkin",
    "Python",
    "Quagga",
    "Rabbit",
    "Raccoon",
    "Rhino",
    "Sheep",
    "Shrew",
    "Skunk",
    "Squirrel",
    "Tiger",
    "Turtle",
    "Walrus",
    "Wolf",
    "Wolverine",
    "Wombat"
  ]



  private static lastTargetId: number = 8;

  /**
   * Returns a random integer between min (inclusive) and max (inclusive).
   * The value is no lower than min (or the next integer greater than min
   * if min isn't an integer) and no greater than max (or the next integer
   * lower than max if max isn't an integer).
   * Using Math.round() will give you a non-uniform distribution!
   */
  private static getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  constructor() { }


}
