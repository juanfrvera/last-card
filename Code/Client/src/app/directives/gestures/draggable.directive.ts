import { AfterViewInit, Directive, ElementRef, EventEmitter, Input, NgZone, Output } from '@angular/core';
import { Gesture, GestureController } from '@ionic/angular';

@Directive({
  selector: '[appDraggable]'
})
export class DraggableDirective implements AfterViewInit {
  /*-----------Interface Variables --------------- */
  @Input() public disableX: boolean;
  @Input() public disableY: boolean;

  @Output() onDragStart: EventEmitter<void> = new EventEmitter();
  @Output() onDragEnd: EventEmitter<void> = new EventEmitter();

  /*-----------Internal Variables ------------- */

  /**Miliseconds in which the power is increased */
  private readonly powerRate = 200;

  /**Accumulated power required for the gesture to be considered a long press */
  private readonly powerToLongPress = 3;

  /**Class that is added to grabbed elements */
  private readonly grabbedClass = "grabbed";

  private pressing = false;

  /**When true, the pointer pressed for the expected time without moving */
  private longPressed = false;
  private power = 0;

  /**When the pointer is moved before a long press, it will not be considered */
  private canceled = false;

  constructor(
    private angularZone: NgZone,
    private el: ElementRef,
    private gestureCtrl: GestureController) {
    const gesture: Gesture = this.gestureCtrl.create({
      el: el.nativeElement,
      gestureName: 'drag',
      threshold: 0,
      onStart: ev => {
        this.pressing = true;
        this.power = 0;
        this.increasePower();
      },
      onMove: ev => {
        if (this.longPressed) {
          el.nativeElement.style.transform = `translate(${this.disableX ? 0 : ev.deltaX}px,${this.disableY ? 0 : ev.deltaY}px)`;
        }
        else {
          // The long press operation will be canceled
          this.canceled = true;
        }
      },
      onEnd: ev => {
        if (this.longPressed) {
          this.dragEnded();
        }

        this.canceled = false;
        this.pressing = false;

        // Used to inform the view the change
        this.angularZone.run(() => {
          this.longPressed = false;
        });
      }
    });
    gesture.enable(true);
  }

  ngAfterViewInit(){
    console.log(this.el.nativeElement.offsetHeight);
  }

  increasePower() {
    console.log(this.power);
    setTimeout(() => {
      if (this.pressing && !this.canceled) {
        // Waiting to start
        if (this.power < this.powerToLongPress) {
          // Reflect changes on angular
          this.angularZone.run(() => {
            this.power++;
            this.increasePower();
          });
        }
        // Start
        else {
          this.dragStarted();
        }
      }
    }, this.powerRate);
  }

  private dragStarted() {
    // Used to inform the view the change
    this.angularZone.run(() => {
      this.longPressed = true;

      //Add grabbed class
      this.el.nativeElement.classList.add(this.grabbedClass);
    });

    // Output the event
    this.onDragStart.emit();
  }

  /**When the element was dragged and the pointer is released */
  private dragEnded() {
    // Remove grabed class
    this.el.nativeElement.classList.remove(this.grabbedClass);

    // Output the event
    this.onDragEnd.emit();
  }

}
