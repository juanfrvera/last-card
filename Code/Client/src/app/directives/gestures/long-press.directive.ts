import { Directive, ElementRef, EventEmitter, Input, NgZone, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Gesture, GestureController } from '@ionic/angular';

@Directive({
  selector: '[appLongPress]'
})
export class LongPressDirective implements OnChanges {

  /** -------Interface Variables --------- */
  @Input() public pressDisabled: boolean;
  /**Accumulated power required for the gesture to be considered a long press */
  @Input("pressDuration") public powerToLongPress = 3;

  @Output() onPress: EventEmitter<void> = new EventEmitter();
  // Useful here because the gestures blocks other events
  @Output("onPressStart") onStart: EventEmitter<void> = new EventEmitter();

  /** -------Internal Variables----------- */
  /**Miliseconds in which the power is increased */
  private readonly powerRate = 80;

  private pressing = false;

  /**When true, the pointer pressed for the expected time without moving */
  private longPressed = false;
  private power = 0;

  /**When the pointer is moved before a long press, it will not be considered */
  private canceled = false;

  // Used to enable or disable the gesture
  private readonly gesture: Gesture;

  constructor(
    private el: ElementRef, private angularZone: NgZone,
    private gestureCtrl: GestureController) {

    this.gesture = this.gestureCtrl.create({
      el: el.nativeElement,
      gestureName: 'long-press',
      threshold: 0,
      onStart: ev => {
        this.pressing = true;
        this.power = 0;
        this.increasePower();

        this.onStart.emit();
      },
      onMove: ev => {
        if (!this.longPressed) {
          // The long press operation will be canceled
          this.canceled = true;
        }
      },
      onEnd: ev => {
        this.canceled = false;
        this.pressing = false;

        // Used to inform the view the change
        this.angularZone.run(() => {
          this.longPressed = false;
        });
      }
    });

    this.gesture.enable(true);
  }

  ngOnChanges(changes: SimpleChanges) {
    // Enable or disable the gesture based on the "disabled" input
    if (changes.pressDisabled) {
      this.gesture.enable(!this.pressDisabled);
    }
  }

  private increasePower() {
    setTimeout(() => {
      if (this.pressing && !this.canceled) {
        // Waiting to start
        if (this.power < this.powerToLongPress) {
          // Reflect changes on angular
          this.angularZone.run(() => {
            this.power++;
            this.increasePower();
          });
        }
        // Start
        else {
          this.fireLongPress();
        }
      }
    }, this.powerRate);
  }

  private fireLongPress() {
    this.longPressed = true;

    // Output the onPress event
    this.onPress.emit();
  }

}

