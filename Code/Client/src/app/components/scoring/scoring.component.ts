import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { IScore } from 'src/app/model/dto/score';
import { AlertService } from 'src/app/services/alert.service';
import { AppService } from '../../services/app.service';

@Component({
  selector: 'app-scoring',
  templateUrl: './scoring.component.html',
  styleUrls: ['./scoring.component.scss'],
})
export class ScoringComponent implements OnInit {

  public manualMode: boolean = false;

  // It can also be an empty string
  public manualScoreText = undefined;

  @ViewChild("manualInput", { static: false }) manualInput: any;

  // Optional parameters to pass to the swiper instance.
  // See http://idangero.us/swiper/api/ for valid options.
  slidesOptions = {
    initialSlide: 5,
    speed: 400,
    centeredSlides: true,
    slideToClickedSlide: true,
    // Default parameters
    slidesPerView: 1,
    spaceBetween: 10,
    // Responsive breakpoints
    breakpoints: {
      200: {
        slidesPerView: 3,
        spaceBetween: 10
      },
      // when window width is >= 320px
      320: {
        slidesPerView: 5,
        spaceBetween: 10
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 7,
        spaceBetween: 10
      },
      // when window width is >= 640px
      640: {
        slidesPerView: 9,
        spaceBetween: 10
      },
      720: {
        slidesPerView: 11,
        spaceBetween: 10
      }
    }
  };
  constructor(private app: AppService, private alert: AlertService) { }

  ngOnInit() { }

  clickManualScore() {
    this.manualMode = true;

    // Wait a little so we get the element reference from the view
    setTimeout(() => {
      this.manualInput.setFocus();
    });
  }

  manualInputBlur() {
    this.manualMode = false;

    if (this.manualScoreText === "") {
      delete this.manualScoreText;
    }
  }

  manualInputChange(value : string) {
    this.manualScoreText = value;

    const numericValue = parseFloat(value);

    const score: IScore = {
      value: numericValue
    }
    this.app.sendCurrentScore(score).catch((error) => {
      this.alert.errorToast("Server error when confirming score");
    });
  }

  currentScore(value: number) {
    const score: IScore = {
      value: value
    }
    this.app.sendCurrentScore(score).catch((error) => {
      this.alert.errorToast("Server error when confirming score");
    });
  }

}


