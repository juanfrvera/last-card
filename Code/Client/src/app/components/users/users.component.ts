import { Component, Input, OnInit } from '@angular/core';
import { IUserData } from '../../model/dto/user-data';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UsersComponent implements OnInit {
  /* --- View Variables ---*/
  @Input('users')
  public users: IUserData[];

  /**Id of the users who scored */
  @Input('scorers')
  public scorers: number[];

  @Input('hostId')
  public hostId: string;

  // Optional parameters to pass to the swiper instance.
  // See http://idangero.us/swiper/api/ for valid options.
  slideOptions = {
    initialSlide: 1,
    speed: 400,
    centerInsufficientSlides: true,
    preloadImages: true,
    updateOnImagesReady: true,
    // Default parameters
    slidesPerView: 1,
    spaceBetween: 10,
    // Responsive breakpoints
    breakpoints: {
      200: {
        slidesPerView: 3,
        spaceBetween: 10
      },
      // when window width is >= 320px
      320: {
        slidesPerView: 4,
        spaceBetween: 20
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 5,
        spaceBetween: 30
      },
      // when window width is >= 640px
      640: {
        slidesPerView: 6,
        spaceBetween: 40
      },
      720: {
        slidesPerView: 7,
        spaceBetween: 50
      }
    }
  };

  constructor() { }

  ngOnInit() {
    console.log("Host id: "+this.hostId);
  }
}
