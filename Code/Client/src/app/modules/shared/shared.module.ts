import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from '../../components/users/users.component';


/**This module is used to import components that are used often in the app */
@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule
  ],
  exports:[UsersComponent]
})
export class SharedModule { }
