# Project: Last Card  

## Authors
- Victoria Derudder
- Fernando Gomez
- Juan Vera

## Description
Last card is a simple tool to make group decisions by determining scores for a predefined target.  
The app starts a room, where users can join. There is a special user called "host" which acts as an admin user and has special privileges.  
The host can select the topic to score and, optionally, set a timer to establish a maximum time to make the ratings.  
When everything is set and ready, the host starts the scoring round, allowing all users to rate the target defined previously.  
Once every user gives their score or the timer is up, the host is able to see all the users' scores and also an average result from them.  
Also, it can work as well with Trello, where a power-up for the app is available. This power-up allows both the host and users to create/join a room.


## About
Desarrollo de aplicaciones Cliente-Servidor

Universidad Tecnológica Nacional  
Facultad regional Concepción del Uruguay
<br >
<br >


**_More information coming up next!_**